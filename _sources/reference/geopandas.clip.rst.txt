geopandas.clip
==============


.. currentmodule:: geopandas

.. autofunction:: clip



.. include:: geopandas.clip.examples

.. raw:: html

    <div class="clear"></div>

