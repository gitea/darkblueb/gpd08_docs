geopandas.GeoDataFrame
======================


.. currentmodule:: geopandas

.. autoclass:: GeoDataFrame



.. include:: geopandas.GeoDataFrame.examples

.. raw:: html

    <div class="clear"></div>

