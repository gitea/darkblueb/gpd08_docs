geopandas.GeoSeries
===================


.. currentmodule:: geopandas

.. autoclass:: GeoSeries



.. include:: geopandas.GeoSeries.examples

.. raw:: html

    <div class="clear"></div>

