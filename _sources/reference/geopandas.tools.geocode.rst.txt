geopandas.tools.geocode
=======================


.. currentmodule:: geopandas.tools

.. autofunction:: geocode



.. include:: geopandas.tools.geocode.examples

.. raw:: html

    <div class="clear"></div>

